# remarkable

ReMarkable is a simple text-editor integrated into Proton designed for extensibility and practicality. 

## Installation

Use Rust's built-in package manager [crates](https://crates.io/crates/package) to install *package*.

```bash
cargo install package
```

## Usage

```rust
use scsys::prelude::*;

fn main() {
  println!("{:?}", Message::<String>::default());
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)